$(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if (scroll >= 50){
        $(".navbar").addClass("colorScroll");
    } else {

        $(".navbar").removeClass("colorScroll");
    }
});

function addColor(){
    if($(".navbar").hasClass("colorScroll")){
        $(".navbar").removeClass("colorScroll");
    }
    else{
        $(".navbar").addClass("colorScroll");
    }

}
